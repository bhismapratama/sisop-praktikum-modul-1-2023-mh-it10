#!/bin/bash

path="genshin_character"

find=0
folder_region=""
jpg_file=""
file=""
nama_file=""
esktrak=""
scan=""
decoded=""

while [ "$find" -eq 0 ]; do
    for folder_region in "$path"/*; do
        if [ "$find" -eq 1 ];then
            break
        fi

        if [ -d "$folder_region" ]; then
            for jpg_file in "$folder_region"/*.jpg; do
                if [ -f "$jpg_file" ]; then
                    file=$(basename "$jpg_file")
                    nama_file="${file%.*}"
                    steghide extract -sf "$jpg_file" -p "" -xf "$nama_file.txt"

                    esktrak="$nama_file.txt"
                    scan=$(cat "$esktrak")
                    decoded=$(echo -n "$scan" | base64 --decode)

                    if [[ "$decoded" == *http* ]]; then
                        echo "[$(date '+%d/%m/%y %H:%M:%S')] [file tidak ditemukan] [$file]" >> "image.log"
                        wget "$decoded"
                        find=1
                        break
                    else
                        echo "[$(date '+%d/%m/%y %H:%M:%S')] [file ditemukan] [$file]" >> "image.log"
                        rm "$esktrak"
                    fi
                fi
            done
        fi
    done
done