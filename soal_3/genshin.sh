#!/bin/bash

wget 'https://drive.google.com/uc?id=1RBuZBtK6q-_I7S_WgeioqIRrUlvRRbq2&export=download' -O genshin.zip

unzip genshin.zip
unzip genshin_character.zip -d genshin_character

folder_path="genshin_character"
csv_file="list_character.csv"

while IFS= read -r -d '' file; do
    if [ -f "$file" ]; then
        nama_file=$(basename -- "$file")
        decoded=$(echo -n "$nama_file" | base64 --decode)
        file_extension="${file##*.}"
        mv "$file" "$folder_path/$decoded.${file_extension}"
    fi
done < <(find "$folder_path" -type f -print0)

IFS=$'\n' read -d '' -a nama_baru < <(tail -n +2 "$csv_file")

while IFS= read -r -d '' file; do
    if [ -f "$file" ]; then
        nama_file=$(basename -- "$file")
        for row in "${nama_baru[@]}"; do
            csv_name=$(echo "$row" | awk -F, '{print $1}')
            if [ "$nama_file" = "$csv_name.jpg" ]; then
                new_name=$(echo "$row" | awk -F, '{print $1 " - " $2 " - " $3 " - " $4}')
                cleaned_name=$(echo "${new_name}" | tr -d '\r')
                file_extension="${file##*.}"
                new_nama_file="$cleaned_name.${file_extension}"
                mv "$file" "$folder_path/$new_nama_file"
                break
            fi
        done
    fi
done < <(find "$folder_path" -type f -print0)

while IFS= read -r -d '' file; do
    if [ -f "$file" ]; then
        region=$(basename "$file" | awk -F ' - ' '{print $2}')
        if [ -n "$region" ]; then
            region_folder="${folder_path}/${region}"

            if [ ! -d "$region_folder" ]; then
                mkdir -p "$region_folder"
            fi

            mv "$file" "${region_folder}/$(basename "$file")"
        fi
    fi
done < <(find "$folder_path" -type f -print0)

awk -F ',' 'NR > 1 {print $4}' 'list_character.csv' | sort | uniq -c 

rm list_character.csv genshin.zip genshin_character.zip