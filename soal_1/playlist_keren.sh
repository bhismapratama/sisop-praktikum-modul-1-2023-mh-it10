#!/bin/bash

#download playlist.csv dulu
wget 'https://drive.google.com/uc?id=1JYkupCAZTmwPZSZRsB61cklPM-LUjRHp&export=download' -O playlist.csv

#menampilkan 5 top teratas lagu dengan genre hip hop berdasarkan popularity
echo -e "Menampilkan 5 top teratas lagu dengan genre hip hop berdasarkan popularity"
awk -F',' 'BEGIN{IGNORECASE=1} /Hip Hop/ {print $0 | "sort -t, -k15,15nr | head -n 5"}' playlist.csv

#mencari 5 lagu yang paling rendah diantara lagu John Mayer berdasarkan popularity
echo -e "\nMenampilkan 5 lagu yang paling rendah diantara lagu john mayer berdasarkan popularity"
awk -F',' 'BEGIN{IGNORECASE=1} /John Mayer/ {print $0 | "sort -t, -k15,15nr | tail -n 5"}' playlist.csv

#mencari 10 lagu pada tahun 2004 dengan ranking popularity tertinggi
echo -e "\nMencari 10 lagu pada tahun 2004 dengan ranking popularity tertinggi"
awk -F',' 'BEGIN{IGNORECASE=1} /2004/ {print $0 | "sort -t, -k15,15nr | head -n 10"}' playlist.csv

#mencari lagu yang diciptakan oleh ibu Sri
echo -e "\nMencari lagu yang diciptakan oleh bu sri"
awk -F',' 'BEGIN{IGNORECASE=1} /sri/' playlist.csv
