#!/bin/bash

waktu_ini=$(date +'%Y%m%d%H%M%S')

mem_info=$(free -m | awk '/Mem/{print $2","$3","$4","$5","$6","$7}')
swap_info=$(free -m | awk '/Swap/{print $2","$3","$4}')

target="/home/bhiss/BHISMA_KULIAH_REAL/SISOP/sisop-praktikum-modul-1-2023-mh-it10/soal_4"
directory_size=$(du -sh "$target" | awk '{print $1}')

header="mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"

output="${mem_info},${swap_info},${target},${directory_size}"

echo "$header" > metrics_$waktu_ini.log
echo "$output" >> metrics_$waktu_ini.log

echo "ada di sini metrics_$waktu_ini.log"
