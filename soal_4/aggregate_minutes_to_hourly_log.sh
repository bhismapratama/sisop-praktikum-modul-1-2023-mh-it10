#!/bin/bash

# Inisialisasi array
declare -a minimum=(9999999 999999 999999 99999 999999 99999 99999 99999 99999)
declare -a maximum=(0 0 0 0 0 0 0 0 0)
declare -a sum=(0 0 0 0 0 0 0 0 0)

direktori="$(pwd)"

if [ ! -d "$direktori" ]; then
    echo "Direktori tidak ditemukan: $direktori"
    exit 1
fi

if [ -z "$(ls -A "$direktori")" ]; then
    echo "Direktori kosong"
    exit 1
fi

min_count=9999
max_count=0
average_count=0

for log in "$direktori"/*; do
    if [ -d "$log" ]; then
        continue
    fi
    
    if [ ! -s "$log" ]; then
        continue
    fi
    
    nama_file=$(basename "$log")
    case "$nama_file" in
        *metrics_agg*) continue ;;
    esac

    data=$(cat "$log")
    IFS=',' read -ra attributes <<< "$data"
    dir="${attributes[9]}"
    path_size="${attributes[10]%M}"
    float_path_size=$(echo "scale=2; $path_size" | bc -l)
    unset attributes[9]

    if [ ${#attributes[@]} -ne 10 ]; then
        echo "File log tidak sesuai format: $log"
        continue
    fi

    for ((i = 0;i < ${#attributes[@]}; i++)); do
        if [[ ${attributes[i]} -lt ${minimum[i]} ]]; then
            minimum[i]=${attributes[i]}
        fi

        if [[ ${attributes[i]} -gt ${maximum[i]} ]]; then
            maximum[i]=${attributes[i]}
        fi

        sum[i]=$((sum[i] + attributes[i]))
    done

    if (( $(echo "$float_path_size > $max_count" | bc -l) )); then
        max_count=$float_path_size
    fi

    if (( $(echo "$float_path_size < $min_count" | bc -l) )); then
        min_count=$float_path_size
    fi

    average_count=$(echo "scale=2; $average_count + $float_path_size" | bc -l)
done

count=$(ls -1 "$direktori"/* | wc -l)

for ((i = 0;i < ${#sum[@]}; i++)); do
    sum[i]=$((sum[i] / count))
done

average_count=$(echo "scale=2; $average_count / $count" | bc -l) 
average_count=$(printf "%.1f" "$average_count")

min=$(echo ${minimum[@]} | awk -F ' ' '{print $1 "," $2 "," $3 ","  $4 "," $5 "," $6 "," $7 "," $8 "," $9 ","}')

max=$(echo ${maximum[@]} | awk -F ' ' '{print $1 "," $2 "," $3 ","  $4 "," $5 "," $6 "," $7 "," $8 "," $9 ","}')

average=$(echo ${sum[@]} | awk -F ' ' '{print $1 "," $2 "," $3 ","  $4 "," $5 "," $6 "," $7 "," $8 "," $9 ","}')

time=$(date '+%Y%m%d%H%M%S')

nama_file="metrics_agg_${time}.log"

echo "minimum,${min}${dir},${min_count}M" > "$direktori/$nama_file"
echo "maximum,${max}${dir},${max_count}M" >> "$direktori/$nama_file"
echo "average,${average}${dir},${average_count}M" >> "$direktori/$nama_file" 
chmod 600 "$direktori/$nama_file"
