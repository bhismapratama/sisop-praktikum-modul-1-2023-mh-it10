#!/bin/bash

#file untuk menyimpan data pengguna dan juga history regis/login
dataUSER="users.txt"
dataLOGIN="auth.log"

# Fungsi untuk melakukan login
login() {
    echo "---------- LOGIN ---------- "
    read -p "Masukkan email: " email
    read -p "Masukkan password: " -s password
    echo

    # Untuk mengecek apakah email yang diinput user  terdaftar dalam file users.txt
    if ! grep -q "$email" "$dataUSER"; then
        echo "TIDAK DAPAT LOGIN - $email TIDAK TERDAFTAR, registrasi terlebih dahulu"
         log "FAILED LOGIN, belum registrasi"
         return
    fi
    stored_password=$(grep "$email" "$dataUSER" | cut -d',' -f3)

    decrypted_password=$(echo -n "$stored_password" | base64 -d)

    # Mengecek password user
    if [ "$password" == "$decrypted_password" ]; then
        username=$(grep "$email" "$dataUSER" | cut -d',' -f2)
        echo "LOGIN BERHASIL - Selamat Datang, $username"
        log "LOGIN $email berhasil"
    else
        echo "LOGIN GAGAL -  Password Salah"
        log "FAILED LOGIN, wrong password"
    fi
}
log() {
    log_time=$(date +"[%d/%m/%y %H:%M:%S]")
    log_message="$1"
    echo "$log_time $log_message" >> "$dataLOGIN"
}

# Memanggil fungsi login agar berjalan 
login
