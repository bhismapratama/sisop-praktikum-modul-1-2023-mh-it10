#!/bin/bash

#file untuk menyimpan data pengguna dan history  registrasi/login
dataUSER="users.txt"
dataLOGIN="auth.log"

# Fungsi untuk melakukan proses registrasi
registrasi() {
    echo "---------- REGISTRASI ----------"
    read -p "Masukkan email: " email
    read -p "Masukkan username: " username
    read -p "Masukkan password: " -s password

    # Mengecek apakah email yang diinputkan user sudah terdaftar
    if grep -q "^$email" "$dataUSER"; then
        printf "\n"
        echo "REGISTRASI GAGAL - Email telah terdaftar."
        return
    fi

    # Mengcek apakah password sudah memenuhi persyaratan
    if [[ ${#password} -lt 8 || !("$password" =~ [A-Z]) || !("$password" =~ [a-z]) || !("$password" =~ [0-9]) || !("$password" =~ [\!\@\#\$\%\^\&\*]) ]]; then
        printf "\n"
        echo "REGISTRASI GAGAL - Password tidak memenuhi persyaratan (LEMAH)."
        log "Registrasi GAGAL"
        return
    fi

    # Mengenkripsi password menggunakan base64
    encrypted_password=$(echo -n "$password" | base64)

    # Menyimpan data pengguna ke file users.txt
    echo "$email,$username,$encrypted_password" >> "$dataUSER"

    echo "REGISTRASI BERHASIL."
    log "Registrasi $email BERHASIL"
}

# Fungsi untuk mencatat log
log() {
    log_time=$(date +"[%d/%m/%y %H:%M:%S]")
    log_message="$1"
    echo "$log_time $log_message" >> "$dataLOGIN"
}
# Memanggil fungsi registrasi agar berjalan 
registrasi

