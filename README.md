## Description

Repository untuk menyimpan **code jawaban praktikum kelompok 10** dari soal-soal Sistem Operasi 2023/2024

Untuk mengakses keperluan, silakan klik **[di sini](https://sisop23.carrd.co)**.

## Praktikan

- [Bhisma Elki Pratama](https://github.com/bhismapratama)
- [Siti Nur Ellyzah](https://github.com/ellyzah)
- [Bintang Ryan Wardana](https://github.com/bintangryan)

main contribution from **[@LABKCKSITS](https://github.com/lab-KCKS)**.

# sisop-praktikum-modul-1-2023-MH-IT10

## Anggota Kelompok

|    NRP     |         Nama          |
| :--------: | :-------------------: |
| 5027221005 |  Bhisma Elki Pratama  |
| 5027221014 |   Siti Nur Ellyzah    |
| 5027221022 | Bintang Ryan Wardhana |

## PERATURAN

1.  Waktu pengerjaan dimulai Senin (18/9) setelah sesi lab hingga Sabtu (23/9) pukul 22.00 WIB.
2.  Praktikan diharapkan membuat laporan penjelasan dan penyelesaian soal dalam bentuk Readme(gitlab).
3.  Format nama repository gitlab “sisop-praktikum-modul-[Nomor Modul]-2023-[Kode Dosen Kelas]-[Nama Kelompok]” (contoh:sisop-praktikum-modul-1-2023-MH-IT01).
4.  Struktur repository seperti berikut:
    - —soal_1:
    -     —playlist_keren.sh
    - —soal_2:
    - —login.sh
    - —register.sh
    - —soal_3:
    - —genshin.sh
    - —find_me.sh
    - —soal_4:
    - —minute_log.sh
    - —aggregate_minutes_to_hourly_log.sh
5.  Jika melanggar struktur repo akan dianggap sama dengan curang dan menerima konsekuensi sama dengan melakukan kecurangan.
6.  Setelah pengerjaan selesai, semua script bash, awk, dan file yang berisi cron job ditaruh di gitlab masing - masing kelompok, dan link gitlab diletakkan pada form yang disediakan. Pastikan gitlab di setting ke publik.
7.  Commit terakhir maksimal 10 menit setelah waktu pengerjaan berakhir. Jika melewati maka akan dinilai berdasarkan commit terakhir.
8.  Jika tidak ada pengumuman perubahan soal oleh asisten, maka soal dianggap dapat diselesaikan.
9.  Jika ditemukan soal yang tidak dapat diselesaikan, harap menuliskannya pada Readme beserta permasalahan yang ditemukan.
10. Praktikan tidak diperbolehkan menanyakan jawaban dari soal yang diberikan kepada asisten maupun praktikan dari kelompok lainnya.
11. Jika ditemukan indikasi kecurangan dalam bentuk apapun di pengerjaan soal shift, maka nilai dianggap 0.
12. Pengerjaan soal shift sesuai dengan modul yang telah diajarkan.
13. Zip dari repository dikirim ke email asisten penguji dengan subjek yang sama dengan nama judul repository, dikirim sebelum deadline dari soal shift.
14. Jika terdapat revisi soal akan dituliskan pada halaman terakhir

## SOAL

1. Farrel ingin membuat sebuah playlist yang sangat edgy di tahun ini. Farrel ingin playlistnya banyak disukai oleh orang-orang lain. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa data untuk membuat playlist terbaik di dunia. Untung saja Farrel menemukan file playlist.csv yang berisi top lagu pada spotify beserta genrenya.
   a. Farrel ingin memasukan lagu yang bagus dengan genre hip hop. Oleh karena itu, Farrel perlu melakukan testi terlebih dahulu. Tampilkan 5 top teratas lagu dengan genre hip hop berdasarkan popularity.
   b. Karena Farrel merasa kurang dengan lagu tadi, dia ingin mencari 5 lagu yang paling rendah diantara lagu ciptaan John Mayer berdasarkan popularity.
   c. Karena Farrel takut lagunya kurang enak didengar dia ingin mencari lagu lagi, cari 10 lagu pada tahun 2004 dengan rank popularity tertinggi
   d. Farrel sangat suka dengan lagu paling keren dan edgy di dunia. Lagu tersebut diciptakan oleh ibu Sri. Bantu Farrel mencari lagu tersebut dengan kata kunci nama pencipta lagu tersebut.

2. Shinichi merupakan seorang detektif SMA yang kembali menjadi anak kecil karena ulah organisasi hitam. Dengan tubuhnya yang mengecil, Shinichi tidak dapat menggunakan identitas lamanya sehingga harus membuat identitas baru. Selain itu, ia juga harus membuat akun baru dengan identitas nya saat ini. Bantu Shinichi membuat program register dan login agar Shinichi dapat dengan mudah membuat semua akun baru yang ia butuhkan.
   a. Shinichi akan melakukan register menggunakan email, username, serta password. Username yang dibuat bebas, namun email bersifat unique.
   b. Shinichi khawatir suatu saat nanti Ran akan curiga padanya dan dapat mengetahui password yang ia buat. Maka dari itu, Shinichi ingin membuat password dengan tingkat keamanan yang tinggi.

   - Password tersebut harus di encrypt menggunakan base64
   - Password yang dibuat harus lebih dari 8 karakter
   - Harus terdapat paling sedikit 1 huruf kapital dan 1 huruf kecil
   - Password tidak boleh sama dengan username
   - Harus terdapat paling sedikit 1 angka
   - Harus terdapat paling sedikit 1 simbol unik
     c. Karena Shinichi akan membuat banyak akun baru, ia berniat untuk menyimpan seluruh data register yang ia lakukan ke dalam folder users file users.txt. Di dalam file tersebut, terdapat catatan seluruh email, username, dan password yang telah ia buat.
     d. Shinichi juga ingin program register yang ia buat akan memunculkan respon setiap kali ia melakukan register. Respon ini akan menampilkan apakah register yang dilakukan Shinichi berhasil atau gagal
     e. Setelah melakukan register, Shinichi akan langsung melakukan login untuk memastikan bahwa ia telah berhasil membuat akun baru. Login hanya perlu dilakukan menggunakan email dan password.
     f. Ketika login berhasil ataupun gagal, program akan memunculkan respon di mana respon tersebut harus mengandung username dari email yang telah didaftarkan. - i. Ex: - 1. LOGIN SUCCESS - Welcome, [username] - 2. LOGIN FAILED - email [email] not registered, please register first
     g. Shinichi juga mencatat seluruh log ke dalam folder users file auth.log, baik login ataupun register. - i. Format: [date] [type] [message] - ii. Type: REGISTER SUCCESS, REGISTER FAILED, LOGIN SUCCESS, LOGIN FAILED - iii. Ex: - 1. [23/09/17 13:18:02] [REGISTER SUCCESS] user [username] registered successfully - 2. [23/09/17 13:22:41] [LOGIN FAILED] ERROR Failed login attempt on user with email [email]

3. Aether adalah seorang gamer yang sangat menyukai bermain game Genshin Impact. Karena hobinya, dia ingin mengoleksi foto-foto karakter Genshin Impact. Suatu saat Pierro memberikannya sebuah Tautan yang berisi koleksi kumpulan foto karakter dan sebuah clue yang mengarah ke endgame. Ternyata setiap nama file telah dienkripsi dengan menggunakan base64. Karena penasaran dengan apa yang dikatakan piero, Aether tidak menyerah dan mencoba untuk mengembalikan nama file tersebut kembali seperti semula.
   a. Aether membuat script bernama genshin.sh, untuk melakukan unzip terhadap file yang telah diunduh dan decode setiap nama file yang terenkripsi dengan base64 . Karena pada file list_character.csv terdapat data lengkap karakter, Aether ingin merename setiap file berdasarkan file tersebut. Agar semakin rapi, Aether mengumpulkan setiap file ke dalam folder berdasarkan region tiap karakter - i. Format: Nama - Region - Elemen - Senjata.jpg
   b. Karena tidak mengetahui jumlah pengguna dari tiap senjata yang ada di folder "genshin_character".Aether berniat untuk menghitung serta menampilkan jumlah pengguna untuk setiap senjata yang ada - i. Format: [Nama Senjata] : [total]
   Untuk menghemat penyimpanan. Aether menghapus file - file yang tidak ia gunakan, yaitu genshin_character.zip, list_character.csv, dan genshin.zip
   c. Namun sampai titik ini Aether masih belum menemukan clue endgame yang disinggung oleh Pierro. Dia berfikir keras untuk menemukan pesan tersembunyi tersebut. Aether membuat script baru bernama find_me.sh untuk melakukan pengecekan terhadap setiap file tiap 1 detik. Pengecekan dilakukan dengan cara meng-ekstrak tiap gambar dengan menggunakan command steghide. Dalam setiap gambar tersebut, terdapat sebuah file txt yang berisi string. Aether kemudian mulai melakukan dekripsi dengan base64 pada tiap file txt dan mendapatkan sebuah url. Setelah mendapatkan url yang ia cari, Aether akan langsung menghentikan program find_me.sh serta mendownload file berdasarkan url yang didapatkan.
   d. Dalam prosesnya, setiap kali Aether melakukan ekstraksi dan ternyata hasil ekstraksi bukan yang ia inginkan, maka ia akan langsung menghapus file txt tersebut. Namun, jika itu merupakan file txt yang dicari, maka ia akan menyimpan hasil dekripsi-nya bukan hasil ekstraksi. Selain itu juga, Aether melakukan pencatatan log pada file image.log untuk setiap pengecekan gambar - i. Format: [date] [type] [image_path] - ii. Ex: - 1. [23/09/11 17:57:51] [NOT FOUND] [image_path] - 2. [23/09/11 17:57:52] [FOUND] [image_path]
   e. Hasil akhir: - genshin_character - find_me.sh - genshin.sh - image.log - [filename].txt - [image].jpg

4. Defatra sangat tergila-gila dengan laptop legion nya. Suatu hari, laptop legion nya mendadak rusak 🙁 Tentu saja, Defatra adalah programer yang harus 24/7 ngoding tanpa menggunakan vscode di laptop legion nya. Akhirnya, dia membawa laptop legion nya ke tukang servis untuk diperbaiki. Setelah selesai diperbaiki, ternyata biaya perbaikan sangat mahal sehingga dia harus menggunakan uang hasil slot nya untuk membayarnya. Menurut Kang Servis, masalahnya adalah pada laptop Defatra yang overload sehingga mengakibatkan crash pada laptop legion nya. Untuk mencegah masalah serupa di masa depan, Defatra meminta kamu untuk membuat program monitoring resource yang tersedia pada komputer.
   Buatlah program monitoring resource pada laptop kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target*path yang akan dimonitor adalah /home/{user}/.
   a. Masukkan semua metrics ke dalam suatu file log bernama metrics*{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2023-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics*20230131150000.log.
   b. Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.
   c. Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2023013115.log dengan format metrics_agg*{YmdH}.log
   d. Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.
   Note: 1. Nama file untuk script per menit adalah minute_log.sh 2. Nama file untuk script agregasi per jam adalah aggregate_minutes_to_hourly_log.sh 3. Semua file log terletak di /home/{user}/log 4. Semua konfigurasi cron dapat ditaruh di file skrip .sh nya masing-masing dalam bentuk comment
   **Berikut adalah contoh isi dari file metrics yang dijalankan tiap menit:** - mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size 15949,10067,308,588,5573,4974,2047,43,2004,/home/youruser/test/,74M
   **Berikut adalah contoh isi dari file aggregasi yang dijalankan tiap jam:** - type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size minimum,15949,10067,223,588,5339,4626,2047,43,1995,/home/user/test/,50M maximum,15949,10387,308,622,5573,4974,2047,52,2004,/home/user/test/,74M average,15949,10227,265.5,605,5456,4800,2047,47.5,1999.5,/home/user/test/,62M

Revisi / Detail Tambahan
- 19 September 2023 3:24 PM WIB 
- Penambahan keterangan untuk soal no 1: semakin besar nilai popularity, semakin populer
- untuk kolom top genre == genre
- sort berdasarkan popularity

- 20 September 2023 11:38 PM WIB - Detail tambahan untuk soal 3
- wget dimasukkan ke dalam script bash (file zip tidak didownload manual)
- Format untuk menampilkan jumlah senjata tidak harus [Nama Senajata] : [Total] yang penting ada nama senjata dan total-nya
- Untuk passphrase pada steghide, bisa dikosongkan saja


## PENJELASAN JAWABAN

### SOAL NO 1 (playlist_keren.sh)

```bash
#!/bin/bash

#download playlist.csv dulu
wget 'https://drive.google.com/uc?id=1JYkupCAZTmwPZSZRsB61cklPM-LUjRHp&export=download' -O playlist.csv

#menampilkan 5 top teratas lagu dengan genre hip hop berdasarkan popularity
echo -e "Menampilkan 5 top teratas lagu dengan genre hip hop berdasarkan popularity"
awk -F',' 'BEGIN{IGNORECASE=1} /Hip Hop/ {print $0 | "sort -t, -k15,15nr | head -n 5"}' playlist.csv

#mencari 5 lagu yang paling rendah diantara lagu John Mayer berdasarkan popularity
echo -e "\nMenampilkan 5 lagu yang paling rendah diantara lagu john mayer berdasarkan popularity"
awk -F',' 'BEGIN{IGNORECASE=1} /John Mayer/ {print $0 | "sort -t, -k15,15nr | tail -n 5"}' playlist.csv

#mencari 10 lagu pada tahun 2004 dengan ranking popularity tertinggi
echo -e "\nMencari 10 lagu pada tahun 2004 dengan ranking popularity tertinggi"
awk -F',' 'BEGIN{IGNORECASE=1} /2004/ {print $0 | "sort -t, -k15,15nr | head -n 10"}' playlist.csv

#mencari lagu yang diciptakan oleh ibu Sri
echo -e "\nMencari lagu yang diciptakan oleh bu sri"
awk -F',' 'BEGIN{IGNORECASE=1} /sri/' playlist.csv
```

![no 1 result](https://raw.githubusercontent.com/bhismapratama/sisop-praktikum-modul-1-2023-MH-IT10/main/praktikum%20soal_1.png)

Dalam mendapatkan jawaban yang ditanyakan pada no 1, pertama dilakukan perintah wget untuk mendownload file yang ingin kita gunakan selama pengerjaan no 1. menggunakan perintah echo -e untuk menampilkan output pada terminal berupa string yang dituliskan, dilanjutkan dengan perintah awk untuk memproses file .csv yang diberikan dan menyesuaikan output yang diinginkan sesuai dengan code yang dituliskan.

syntax = awk -F',' 'BEGIN{IGNORECASE=1} /Hip Hop/ {print $0 | "sort -t, -k15,15nr | head -n 5"}' playlist.csv
**Penjelasan dari salah satu syntax yang digunakan**

- awk merupakan perintah yang digunakan untuk memanipulasi dan melakukan proses data dalam file (playlist.csv)
- -F ',' merupakan pemisah antar kolom dalam file (menggunakan pemisah koma ',')
- BEGIN{IGNORECASE=1} merupakan syntax untuk pencocokan data tanpa memperhatikan uppercase atau lowercase
- /Hip Hop/ merupakan pencarian untuk pencocokan data yang memiliki nama Hip Hop
- {print $0 | "sort -t, -k15,15nr | head -n 10"} merupakan hal yang dieksekusi ketika baris sudah sesuai dengan pola pencarian
- sort merupakan perintah untuk urutan, dan urutan tersebut berdasarkan kolom ke 15 secara numerik dengan indikasi pengurutan terbalik

Rata-rata untuk pola syntax merupakan hal yang sama dalam segi penjelasan baik itu pada poin subsoal 2 dan 3, dilakukan hal yang hampir sama dengan subsoal 1 namun ditambahkan sorting yang disesuaikan terhadap kasus nya masing-masing ( 5 lagu yang paling rendah diantara lagu John Mayer berdasarkan popularity dan mencari 10 lagu pada tahun 2004 dengan ranking popularity tertinggi ).Pada subsoal 4, dilakukan hal yang sama dengan subsoal 1 namun kali ini baris data yang dicari adalah baris data yang mengandung kata "sri" karena dilakukan pencarian lagu yang diciptakan oleh ibu sri sehingga pada perintah awk -F ',' akan dikurangi dalam segi sort maupun head tetapi dituliskan setelah BEGIN{IGNORECASE=1} yakni '/sri/' sebagai properti pencarian.

### SOAL NO 2 (REGISTER.SH DAN LOGIN.SH)

Pada soal no 2, kita diminta untuk membuat program register dan login dengan menggunakan bash. Kedua program yaitu program register dan login dipisah menjadi 2 file bash yang berbeda yakni register.sh dan login.sh. Kedua file bash itu disimpan/dibuat dalam direktori sisop yang ada di direktori Documents pada direktori user. Berikut adalah penjelasan tiap persyaratan program yang diminta pada soal no 2 :

**A. Shinichi akan melakukan register menggunakan email, username, serta password. Username yang dibuat bebas, namun email bersifat unique.**

# Fungsi untuk melakukan proses registrasi

registrasi() {
echo "---------- REGISTRASI ----------"
read -p "Masukkan email: " email
read -p "Masukkan username: " username
read -p "Masukkan password: " -s password

    # Mengecek apakah email yang diinputkan user sudah terdaftar
    if grep -q "^$email" "$dataUSER"; then
        printf "\n"
        echo "REGISTRASI GAGAL - Email telah terdaftar."
        return
    fi

Pada fungsi utama registrasi pada register.sh, user diminta untuk input email, uname dan juga passwordnya, kemudian untuk memenuhi permintaan soal 2A, dibuat sebuah if-else agar email yang dimasukkan bersifat unique dengan mengecek email terdaftar pada dataUSER dengan command grep -q

**B. Shinichi khawatir suatu saat nanti Ran akan curiga padanya dan dapat mengetahui password yang ia buat. Maka dari itu, Shinichi ingin membuat password dengan tingkat keamanan yang tinggi.**
-Password tersebut harus di encrypt menggunakan base64
-Password yang dibuat harus lebih dari 8 karakter
-Harus terdapat paling sedikit 1 huruf kapital dan 1 huruf kecil
-Password tidak boleh sama dengan username
-Harus terdapat paling sedikit 1 angka
-Harus terdapat paling sedikit 1 simbol unik

# Mengenkripsi password menggunakan base64

    encrypted_password=$(echo -n "$password" | base64)

Itu adalah perintah untuk mengenkripsi pwd yang diinput user menggunakan base64. Sementara persyaratan pwd lainnya diatur pada bagian berikut :

# Mengcek apakah password sudah memenuhi persyaratan

    if [[ ${#password} -lt 8 || !("$password" =~ [A-Z]) || !("$password" =~ [a-z]) || !("$password" =~ [0-9])
        || !("$password" =~ [\!\@\#\$\%\^\&\*]) ]]; then
        printf "\n"
        echo "REGISTRASI GAGAL - Password tidak memenuhi persyaratan (LEMAH)."
        log "Registrasi GAGAL"
        return
    fi

Pada bagian tersebut, pwd yang diinput user akan dicek apakah memenuhi persyaratan yang sesuai pada soal 2B, mulai dari syarat minimal 8 karakter, harus ada kapital dan syarat lainnya. Apabila user melanggar salah satu syarat, maka registrasi akan gagal. Bagian ini menggunakan if-else dan juga simbol "||" sebagai operator atau. -lt 8 untuk mengecek jumlah karakter, [A-Z] untuk mengecek apakah ada huruf kapital antara a sampai z, dan begitu pula untuk persyaratan lainnya.

**C. Karena Shinichi akan membuat banyak akun baru, ia berniat untuk menyimpan seluruh data register yang ia lakukan ke dalam folder users file users.txt. Di dalam file tersebut, terdapat catatan seluruh email, username, dan password yang telah ia buat.**

    # Menyimpan data pengguna ke file users.txt
    echo "$email,$username,$encrypted_password" >> "$dataUSER"

Pada register.sh dibuat juga perintah untuk menyimpan data registrasi user yaitu email, pwd, dan uname ke dalam file berisi seluruh data user yaitu user.txt. Dalam hal ini, dataUSER dideklarasikan sebagai user.txt, itu ada pada awal bagian program register.sh dan login.sh yaitu :

    #file untuk menyimpan data pengguna dan history  registrasi/login
    dataUSER="users.txt"
    dataLOGIN="auth.log"

**D. Shinichi juga ingin program register yang ia buat akan memunculkan respon setiap kali ia melakukan register. Respon ini akan menampilkan apakah register yang dilakukan Shinichi berhasil atau gagal**

    echo "REGISTRASI GAGAL - Email telah terdaftar."
    echo "REGISTRASI GAGAL - Password tidak memenuhi persyaratan (LEMAH)."
    echo "REGISTRASI BERHASIL."

Pada bagian if-else untuk mengecek persyaratan, program akan print output keterangan apakah proses tersebut berhasil, pertama pengecekan apakah email unique, kedua pengecekan pwd, dan terakhir adalah hasil akhirnya apakah memenuhi syarat registrasi, jika terpenuhi maka program akan memberikan ouput "REGISTRASI BERHASIL"

**E. Setelah melakukan register, Shinichi akan langsung melakukan login untuk memastikan bahwa ia telah berhasil membuat akun baru. Login hanya perlu dilakukan menggunakan email dan password.**

---------- LOGIN ----------
Masukkan email: shinichi@mail.com
Masukkan password:
TIDAK DAPAT LOGIN - shinichi@mail.com TIDAK TERDAFTAR, registrasi terlebih dahulu

Itu adalah tampilan yang muncul di layar user apabila menjalankan program login.sh, Terlihat jelas bahwa pada proses login, user hanya diminta untuk menginputkan email dan juga pwd tanpa input uname.

**F. Ketika login berhasil ataupun gagal, program akan memunculkan respon di mana respon tersebut harus mengandung username dari email yang telah didaftarkan.**

---------- LOGIN ----------
Masukkan email: shinichi@gmail.com
Masukkan password:
LOGIN BERHASIL - Selamat Datang, shinichi

Itu adalah tampilan jika login berhasil, jika berhasil maka akan ada keterangan Selamat datang uname, dalam hal ini adalah shinichi. Berikut adalah bagian yang mengatur hal ini pada bash login.sh :

    # Mengecek password user
    if [ "$password" == "$decrypted_password" ]; then
        username=$(grep "$email" "$dataUSER" | cut -d',' -f2)
        echo "LOGIN BERHASIL - Selamat Datang, $username"
        log "LOGIN $email berhasil"
    else

**G. Shinichi juga mencatat seluruh log ke dalam folder users file auth.log, baik login ataupun register.**

log() {
log_time=$(date +"[%d/%m/%y %H:%M:%S]")
    log_message="$1"
    echo "$log_time $log_message" >> "$dataLOGIN"
}

Pada register.sh maupun login.sh, dibuat fungsi log untuk mencatat history aktivitas user, baik tindakan registrasi atau pun login. Bisa dilihat papda fungsi log itu, ada log_message yang nantinya dideklarasikan sebagai argumen pertama pada setiap kondisi. Contohnya seperti kondisi berikut :

# Untuk mengecek apakah email yang diinput user terdaftar dalam file users.txt

    if ! grep -q "$email" "$dataUSER"; then
        echo "TIDAK DAPAT LOGIN - $email TIDAK TERDAFTAR, registrasi terlebih dahulu"
         log "FAILED LOGIN, belum registrasi"
         return
    fi

Jika kondisi terpenuhi, maka fungsi log dipanggil dan log_message dideklarasikan sebagai "FAILED LOGIN, belum registrasi" yang nantinya akan disimpan dalam dataLOGIN yang dideklarasikan sebagai file auth.log pada awal program. Fungsi log ini bukan hanya dipanggil pada kondisi itu, tetapi di setiap kondisi yang mungkin terjadi pada program register.sh dan login.sh. Berikut adalah tampilan yang muncul jika membuka file auth.log dengan command cat auth.log :

[29/09/23 22:12:13] Registrasi GAGAL
[29/09/23 22:12:40] Registrasi GAGAL
[29/09/23 22:12:52] Registrasi GAGAL
[29/09/23 22:13:14] Registrasi shinichi@gmail.com BERHASIL
[29/09/23 22:13:39] FAILED LOGIN, belum registrasi
[29/09/23 22:13:54] LOGIN shinichi@gmail.com berhasil

Semua data user pada akhirnya akan disimpan dalam user.txt dan histori aktivitas registrasi dan login akan disimpan dalam file auth.log.

beberapa dokumentasi selama pengerjaan berlangsung

**login**
![no 2 result](https://raw.githubusercontent.com/bhismapratama/sisop-praktikum-modul-1-2023-MH-IT10/main/soal2_loginsh.png)

**auth.log**
![no 2 result](https://raw.githubusercontent.com/bhismapratama/sisop-praktikum-modul-1-2023-MH-IT10/main/soal2_authLog.png)

**Register.sh**
![no 2 result](https://raw.githubusercontent.com/bhismapratama/sisop-praktikum-modul-1-2023-MH-IT10/main/soal2_registersh.png)

**user.txt**
![no 2 result](https://raw.githubusercontent.com/bhismapratama/sisop-praktikum-modul-1-2023-MH-IT10/main/soal2_userTxt.png)


### SOAL NO 3 (GENSHIN.SH & FIND_ME.SH)

**SOAL NO 3**

```bash
#!/bin/bash

wget 'https://drive.google.com/uc?id=1RBuZBtK6q-_I7S_WgeioqIRrUlvRRbq2&export=download' -O genshin.zip

unzip genshin.zip
unzip genshin_character.zip -d genshin_character

folder_path="genshin_character"
csv_file="list_character.csv"

while IFS= read -r -d '' file; do
    if [ -f "$file" ]; then
        nama_file=$(basename -- "$file")
        decoded=$(echo -n "$nama_file" | base64 --decode)
        file_extension="${file##*.}"
        mv "$file" "$folder_path/$decoded.${file_extension}"
    fi
done < <(find "$folder_path" -type f -print0)

IFS=$'\n' read -d '' -a nama_baru < <(tail -n +2 "$csv_file")

while IFS= read -r -d '' file; do
    if [ -f "$file" ]; then
        nama_file=$(basename -- "$file")
        for row in "${nama_baru[@]}"; do
            csv_name=$(echo "$row" | awk -F, '{print $1}')
            if [ "$nama_file" = "$csv_name.jpg" ]; then
                new_name=$(echo "$row" | awk -F, '{print $1 " - " $2 " - " $3 " - " $4}')
                cleaned_name=$(echo "${new_name}" | tr -d '\r')
                file_extension="${file##*.}"
                new_nama_file="$cleaned_name.${file_extension}"
                mv "$file" "$folder_path/$new_nama_file"
                break
            fi
        done
    fi
done < <(find "$folder_path" -type f -print0)

while IFS= read -r -d '' file; do
    if [ -f "$file" ]; then
        region=$(basename "$file" | awk -F ' - ' '{print $2}')
        if [ -n "$region" ]; then
            region_folder="${folder_path}/${region}"

            if [ ! -d "$region_folder" ]; then
                mkdir -p "$region_folder"
            fi

            mv "$file" "${region_folder}/$(basename "$file")"
        fi
    fi
done < <(find "$folder_path" -type f -print0)

awk -F ',' 'NR > 1 {print $4}' 'list_character.csv' | sort | uniq -c

rm list_character.csv genshin.zip genshin_character.zip
```

**SOAL 3A dan SOAL 3B**
pada poin a nomor 3 kita diminta untuk memberikan nama file dengan nama genshin.sh kemudian didalam script tersebut dilakukan wget untuk melakukan download pada file yang telah disediakan dalam soal.
Bukan hanya itu, setelah menuliskan untuk melakukan wget pada link file yang telah ditentukan, dilakukan unzip file genshin.zip serta unzip genshin_character.zip, yang nantinya akan didapatkan file baru dengan nama genshin_character dan list_character.csv

- Kemudian kita melakukan decode pada file jpg yang berada pada folder genshin_character

```bash
    while IFS= read -r -d '' file; do
    if [ -f "$file" ]; then
        nama_file=$(basename -- "$file")
        decoded=$(echo -n "$nama_file" | base64 --decode)
        file_extension="${file##*.}"
        mv "$file" "$folder_path/$decoded.${file_extension}"
    fi
done < <(find "$folder_path" -type f -print0)
```

kemudian ganti nama sesuai list_character.csv

```bash
IFS=$'\n' read -d '' -a nama_baru < <(tail -n +2 "$csv_file")

while IFS= read -r -d '' file; do
    if [ -f "$file" ]; then
        nama_file=$(basename -- "$file")
        for row in "${nama_baru[@]}"; do
            csv_name=$(echo "$row" | awk -F, '{print $1}')
            if [ "$nama_file" = "$csv_name.jpg" ]; then
                new_name=$(echo "$row" | awk -F, '{print $1 " - " $2 " - " $3 " - " $4}')
                cleaned_name=$(echo "${new_name}" | tr -d '\r')
                file_extension="${file##*.}"
                new_nama_file="$cleaned_name.${file_extension}"
                mv "$file" "$folder_path/$new_nama_file"
                break
            fi
        done
    fi
done < <(find "$folder_path" -type f -print0)
```

serta disesuaikan dengan region yang ada berdasarkan instruksi yang diberikan

```bash
while IFS= read -r -d '' file; do
    if [ -f "$file" ]; then
        region=$(basename "$file" | awk -F ' - ' '{print $2}')
        if [ -n "$region" ]; then
            region_folder="${folder_path}/${region}"

            if [ ! -d "$region_folder" ]; then
                mkdir -p "$region_folder"
            fi

            mv "$file" "${region_folder}/$(basename "$file")"
        fi
    fi
done < <(find "$folder_path" -type f -print0)

awk -F ',' 'NR > 1 {print $4}' 'list_character.csv' | sort | uniq -c

rm list_character.csv genshin.zip genshin_character.zip
```

**SOAL 3C dan SOAL 3D**

```bash
#!/bin/bash

path="genshin_character"

find=0
folder_region=""
jpg_file=""
file=""
nama_file=""
esktrak=""
scan=""
decoded=""

while [ "$find" -eq 0 ]; do
    for folder_region in "$path"/*; do
        if [ "$find" -eq 1 ];then
            break
        fi

        if [ -d "$folder_region" ]; then
            for jpg_file in "$folder_region"/*.jpg; do
                if [ -f "$jpg_file" ]; then
                    file=$(basename "$jpg_file")
                    nama_file="${file%.*}"
                    steghide extract -sf "$jpg_file" -p "" -xf "$nama_file.txt"

                    esktrak="$nama_file.txt"
                    scan=$(cat "$esktrak")
                    decoded=$(echo -n "$scan" | base64 --decode)

                    if [[ "$decoded" == *http* ]]; then
                        echo "[$(date '+%d/%m/%y %H:%M:%S')] [file tidak ditemukan] [$file]" >> "image.log"
                        wget "$decoded"
                        find=1
                        break
                    else
                        echo "[$(date '+%d/%m/%y %H:%M:%S')] [file ditemukan] [$file]" >> "image.log"
                        rm "$esktrak"
                    fi
                fi
            done
        fi
    done
done
```

**PENJELASAN**
dari pertanyaan dijelaskan bahwa sampai titik ini Aether masih belum menemukan clue endgame yang disinggung oleh Pierro. Dia berfikir keras untuk menemukan pesan tersembunyi tersebut. Aether membuat script baru bernama find_me.sh untuk melakukan pengecekan terhadap setiap file tiap 1 detik
kita diminta untuk membuat script bernama find_me.sh, Aether diminta melakukan pengecekan pada setiap file jpg di dalam folder genshin_character dengan menggunakan steghide dan akan mendapatkan file .txt dari pengekstrakan tersebut.

```bash
#!/bin/bash:
Ini adalah shebang, yang memberi tahu sistem bahwa skrip akan dijalankan dengan menggunakan Bash.
```

```bash
path="genshin_character":
find=0:
folder_region="", jpg_file="", file="", nama_file="", esktrak="", scan="", decoded="":
merupakan Inisialisasi variabel-variabel yang akan digunakan dalam proses pencarian dan ekstraksi.
```

```bash
while [ "$find" -eq 0 ]; do ... done:
for folder_region in "$path"/*; do ... done:

merupakan loop yang dijalankan sesuai dengan kondisi tertentu (di mana while tetap berjalan selama find == 0 dan for iterasi kerika dalam direktori yang ditentukan path)
```

```bash
if [ -f "$jpg_file" ]; then
    file=$(basename "$jpg_file")
    nama_file="${file%.*}"
    steghide extract -sf "$jpg_file" -p "" -xf "$nama_file.txt"

    esktrak="$nama_file.txt"
    scan=$(cat "$esktrak")
    decoded=$(echo -n "$scan" | base64 --decode)

    if [[ "$decoded" == *http* ]]; then
        echo "[$(date '+%d/%m/%y %H:%M:%S')] [file tidak ditemukan] [$file]" >> "image.log"
        wget "$decoded"
        find=1
        break
    else
        echo "[$(date '+%d/%m/%y %H:%M:%S')] [file ditemukan] [$file]" >> "image.log"
        rm "$esktrak"
    fi
fi
```

dalam kode tersebut menjelasan mulai dari cakupan kondisi yang diiterasi pada sebuah file (-f mengecek apakah item adalah file jpg) kemudian ekstrak nama file dari nama_filenya, tak hanya itu pada steghide pun diberikan perintah untuk mengekstrak data tersembunyi dari file gambar yang nantinya hasil ekstraksi tersebut akan disimpan dalam file dengan nama yang sama seperti file gambar, namun dengan ektensi .txt.

adapun dokumentasi pendukung ketika kami melakukan running pada kode yang telah dituliskan

**genshin.sh(1)**
![no 3 result](https://raw.githubusercontent.com/bhismapratama/sisop-praktikum-modul-1-2023-MH-IT10/main/praktikum_soal3_genshin1.png)

**genshin.sh (folder)**
![no 3 result](https://raw.githubusercontent.com/bhismapratama/sisop-praktikum-modul-1-2023-MH-IT10/main/praktikum_soal3_genshin.png)

**find_me**
![no 3 result](https://raw.githubusercontent.com/bhismapratama/sisop-praktikum-modul-1-2023-MH-IT10/main/praktikum_soal3_find_me.png)

**all folder**
![no 3 result](https://raw.githubusercontent.com/bhismapratama/sisop-praktikum-modul-1-2023-MH-IT10/main/praktikum_soal3_allFolder.png)


### SOAL NO 4 (MINUTE_LOG.SH & AGGREGATE_MINUTES_TO_HOURLY_LOG.SH)

**SOAL NO 4A dan 4B**

```bash
#!/bin/bash

waktu_ini=$(date +'%Y%m%d%H%M%S')

mem_info=$(free -m | awk '/Mem/{print $2","$3","$4","$5","$6","$7}')
swap_info=$(free -m | awk '/Swap/{print $2","$3","$4}')

target="/home/bhiss/BHISMA_KULIAH_REAL/SISOP/sisop-praktikum-modul-1-2023-mh-it10/soal_4"
directory_size=$(du -sh "$target" | awk '{print $1}')

header="mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"

output="${mem_info},${swap_info},${target},${directory_size}"

echo "$header" > metrics_$waktu_ini.log
echo "$output" >> metrics_$waktu_ini.log

echo "ada di sini metrics_$waktu_ini.log"
```

Pada soal ini kita diberikan case untuk membuat suatu program yang dapat memasukkan semua metrics ke dalam suatu file log bernama metrics\_{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash dijalankan. Misalkan dijalankan pada 2023-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20230131150000.log.

pada poin 4a dengan penjelasan struktur code seperti ini

```bash
waktu_ini=$(date +'%Y%m%d%H%M%S')
variable waktu_ini digunakan untuk mendapatkan timestamp saat ini dalam format YYYYMMDDHHMMSS (tahun, bulan, hari, jam, menit, detik).
```

```bash
mem_info=$(free -m | awk '/Mem/{print $2","$3","$4","$5","$6","$7}')
variable yang digunakan untuk mendapatkan informasi tentang penggunaan memori dari perintah free -m. Output dari perintah tersebut menggunakan awk untuk mencetak kolom yang berisi informasi penting tentang memori
```

```bash
swap_info=$(free -m | awk '/Swap/{print $2","$3","$4}')
variable yang digunakan untuk mendapatkan informasi tentang penggunaan swap.
```

```bash
target="/home/bhiss/BHISMA_KULIAH_REAL/SISOP/sisop-praktikum-modul-1-2023-mh-it10/soal_4"
merupakan variabel target menyimpan jalur menuju direktori yang ingin dilihat sizenya.
```

```bash
directory_size=$(du -sh "$target" | awk '{print $1}')
variable yang digunakan untuk mendapatkan size direktori yang disimpan dalam variabel target.
output menggunakan awk untuk mencetak kolom pertama, yang berisi size direktori.
```

```bash
header="mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"
varianle header yang merupakan string yang berisi data yang akan disimpan dalam file log serta ditampilkan pada layar terminal.
```

```bash
output="${mem_info},${swap_info},${target},${directory_size}"
Variabel output yang menyimpan informasi sistem yang sudah dikumpulkan.
```

```bash
echo "$header" > metrics_$waktu_ini.log
memberikan keterangan header dan menuliskan dalam file log dengan nama yang terdiri dari "metrics_" diikuti dengan timestamp yang telah diambil sebelumnya.
```

```bash
echo "$output" >> metrics_$waktu_ini.log
Menulis data yang sudah dikumpulkan ke dalam file log yang sama. Tanda >> digunakan untuk menambahkan data ke akhir file tanpa menghapus konten yang sudah ada.
```

```bash
echo "ada di sini metrics_$waktu_ini.log"
echo yang digunakan untuk mencetak pesan yang memberitahu pengguna bahwa file log sudah tersimpan di suatu lokasi.
```

bisa disimpulkan bahwa kode ini mengumpulkan informasi tentang penggunaan memori dan swap, ukuran dari direktori, dan menyimpannya dalam sebuah file log dengan timestamp di nama file tersebut. Informasi tersebut dapat berguna untuk memantau kinerja sistem pada waktu tertentu.

Dengan script yang digunakan, diperlukan catatan metrics yang dapat berjalan secara otomatis setiap menit, sehingga kami dapat menggunakan perintah crontab -e dan menambahkan #* * * * * /path/minute_log.sh dimana path merupakan alamat lengkap dari direktori dimana minute_log akan dijalankan setiap menitnya.

**SOAL NO 4C dan 4D**

```bash

#!/bin/bash

declare -a minimum=(9999999 999999 999999 99999 999999 99999 99999 99999 99999)
declare -a maximum=(0 0 0 0 0 0 0 0 0)
declare -a sum=(0 0 0 0 0 0 0 0 0)

direktori="$(pwd)"

if [ ! -d "$direktori" ]; then
    echo "Direktori tidak ditemukan: $direktori"
    exit 1
fi

if [ -z "$(ls -A "$direktori")" ]; then
    echo "Direktori kosong"
    exit 1
fi

min_count=9999
max_count=0
average_count=0

for log in "$direktori"/*; do
    if [ -d "$log" ]; then
        continue
    fi

    if [ ! -s "$log" ]; then
        continue
    fi

    nama_file=$(basename "$log")
    case "$nama_file" in
        *metrics_agg*) continue ;;
    esac

    data=$(cat "$log")
    IFS=',' read -ra attributes <<< "$data"
    dir="${attributes[9]}"
    path_size="${attributes[10]%M}"
    float_path_size=$(echo "scale=2; $path_size" | bc -l)
    unset attributes[9]

    if [ ${#attributes[@]} -ne 10 ]; then
        echo "File log tidak sesuai format: $log"
        continue
    fi

    for ((i = 0;i < ${#attributes[@]}; i++)); do
        if [[ ${attributes[i]} -lt ${minimum[i]} ]]; then
            minimum[i]=${attributes[i]}
        fi

        if [[ ${attributes[i]} -gt ${maximum[i]} ]]; then
            maximum[i]=${attributes[i]}
        fi

        sum[i]=$((sum[i] + attributes[i]))
    done

    if (( $(echo "$float_path_size > $max_count" | bc -l) )); then
        max_count=$float_path_size
    fi

    if (( $(echo "$float_path_size < $min_count" | bc -l) )); then
        min_count=$float_path_size
    fi

    average_count=$(echo "scale=2; $average_count + $float_path_size" | bc -l)
done

count=$(ls -1 "$direktori"/* | wc -l)

for ((i = 0;i < ${#sum[@]}; i++)); do
    sum[i]=$((sum[i] / count))
done

average_count=$(echo "scale=2; $average_count / $count" | bc -l)
average_count=$(printf "%.1f" "$average_count")

min=$(echo ${minimum[@]} | awk -F ' ' '{print $1 "," $2 "," $3 ","  $4 "," $5 "," $6 "," $7 "," $8 "," $9 ","}')

max=$(echo ${maximum[@]} | awk -F ' ' '{print $1 "," $2 "," $3 ","  $4 "," $5 "," $6 "," $7 "," $8 "," $9 ","}')

average=$(echo ${sum[@]} | awk -F ' ' '{print $1 "," $2 "," $3 ","  $4 "," $5 "," $6 "," $7 "," $8 "," $9 ","}')

time=$(date '+%Y%m%d%H%M%S')

nama_file="metrics_agg_${time}.log"

echo "minimum,${min}${dir},${min_count}M" > "$direktori/$nama_file"
echo "maximum,${max}${dir},${max_count}M" >> "$direktori/$nama_file"
echo "average,${average}${dir},${average_count}M" >> "$direktori/$nama_file"
chmod 600 "$direktori/$nama_file"

```

Tak hanya itu pada waktu jam pun supaya script dapat bekerja secara otomatis, kami juga menggunakan crontab -e serta menambahkan #0 * * * * /path/aggregate_minutes_to_hourly_log dimana path merupakan alamat lengkap dari direktori dimana aggregate_minutes_to_hourly_log akan dijalankan setiap menitnya.

Berelasi pada crontab -e tersebut, pada poin soal kali ini kita diminta untuk membuat agregasi file log ke satuan jam dengan membuat file aggregate_minutes_to_hourly_log

Selanjutnya dilakukan scripting bash menggunakan #!/bin/bash, serta menambahkan kode untuk memastikan bahwa agregasi akan disimpan ke dalam direktori.

kemudian kami mencoba menambahkan declare variable angka minimum, maximum serta rata-rata. dengan mencoba perhitungan dari semua metrics per menit yang telah dijalankan.

```bash

declare -a minimum=(9999999 999999 999999 99999 999999 99999 99999 99999 99999)
declare -a maximum=(0 0 0 0 0 0 0 0 0)
declare -a sum=(0 0 0 0 0 0 0 0 0)

```

kemudian dilakukan perulangan yang sekaligus melakukan pengecekan terhadap nama file apabila nama_file tersebut terdapat metrics_agg

```bash
for log in "$direktori"/*; do
    if [ -d "$log" ]; then
        continue
    fi

    if [ ! -s "$log" ]; then
        continue
    fi

    nama_file=$(basename "$log")
    case "$nama_file" in
        *metrics_agg*) continue ;;
    esac
```

adapun penyebutan array atributes yang menjadi sasaran pokok dalam identifikasi dierktori seperti pada kode ini

```bash
data=$(cat "$log")
    IFS=',' read -ra attributes <<< "$data"
    dir="${attributes[9]}"
    path_size="${attributes[10]%M}"
    float_path_size=$(echo "scale=2; $path_size" | bc -l)
    unset attributes[9]
```

selanjutnya dilakukan perulangan dan pengkondisian untuk menentukan nilai maksimum, minimum dan rata-rata dari file menit yang diproses

```bash

for ((i = 0;i < ${#attributes[@]}; i++)); do
        if [[ ${attributes[i]} -lt ${minimum[i]} ]]; then
            minimum[i]=${attributes[i]}
        fi

        if [[ ${attributes[i]} -gt ${maximum[i]} ]]; then
            maximum[i]=${attributes[i]}
        fi

        sum[i]=$((sum[i] + attributes[i]))
    done

    if (( $(echo "$float_path_size > $max_count" | bc -l) )); then
        max_count=$float_path_size
    fi

    if (( $(echo "$float_path_size < $min_count" | bc -l) )); then
        min_count=$float_path_size
    fi

    average_count=$(echo "scale=2; $average_count + $float_path_size" | bc -l)
```

kemudian dari hasil tersebut dilakukan cetak untuk mendapatkan nilai yang diinginkan atau value output yang sesuai

```bash

average_count=$(echo "scale=2; $average_count / $count" | bc -l)
average_count=$(printf "%.1f" "$average_count")

min=$(echo ${minimum[@]} | awk -F ' ' '{print $1 "," $2 "," $3 ","  $4 "," $5 "," $6 "," $7 "," $8 "," $9 ","}')

max=$(echo ${maximum[@]} | awk -F ' ' '{print $1 "," $2 "," $3 ","  $4 "," $5 "," $6 "," $7 "," $8 "," $9 ","}')

average=$(echo ${sum[@]} | awk -F ' ' '{print $1 "," $2 "," $3 ","  $4 "," $5 "," $6 "," $7 "," $8 "," $9 ","}')

time=$(date '+%Y%m%d%H%M%S')

nama_file="metrics_agg_${time}.log"

echo "minimum,${min}${dir},${min_count}M" > "$direktori/$nama_file"
echo "maximum,${max}${dir},${max_count}M" >> "$direktori/$nama_file"
echo "average,${average}${dir},${average_count}M" >> "$direktori/$nama_file"

```

Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file, maka ditambahkan code 

```bash
chmod 600 "$direktori/$nama_file"
```

beberapa dokumentasi pengerjaan soal dan output soal yang kami kerjakan dan run sesuai dengan code yang kami gunakan

**all folder**
![no 4 result](https://raw.githubusercontent.com/bhismapratama/sisop-praktikum-modul-1-2023-MH-IT10/main/soal4_allfile.png)


## KENDALA
adapun kendala yang kami alami ketika melakukan running pada poin 4 aggregate_minutes_to_hourly_log
![no 4 result](https://raw.githubusercontent.com/bhismapratama/sisop-praktikum-modul-1-2023-MH-IT10/main/soal4_metrics_agg.png)






